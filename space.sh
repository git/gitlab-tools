#!/bin/bash

REPOSITORIES=/home/git/repositories/*
# in Megabytes
MAX_SIZE=25

for OWNER in $REPOSITORIES
do
	NAME=`echo $OWNER | cut -d'/' -f5`
	SIZE=`du -c $OWNER | grep total | cut -f1`

	if [ "$SIZE" -gt "$(($MAX_SIZE * 1024))" ]; then
		echo "$NAME: $(($SIZE / 1024))MB"
	fi
done

